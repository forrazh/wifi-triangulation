#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![allow(async_fn_in_trait)]

use core::f64::consts::PI;
use core::str::from_utf8;

use cyw43_pio::PioSpi;
use defmt::{println, unwrap};
use defmt::export::f32;
use embassy_net::tcp::{Error, TcpSocket};
use embassy_net::{Config, Stack, StackResources};
use embassy_rp::gpio::{Input, Level, Output, Pin, Pull};
use embedded_io_async::Write;
use static_cell::make_static;
use embassy_executor::Spawner;
use embassy_rp::adc::Adc;
use embassy_rp::bind_interrupts;
use embassy_rp::peripherals::{DMA_CH0, PIN_23, PIN_25, PIO0, USB};
use embassy_rp::pio::{InterruptHandler as PioInterruptHandler, Pio};
use embassy_rp::usb::{Driver as UsbDriver, InterruptHandler as UsbInterruptHandler};
use embassy_time::{Duration, Timer};
use libm::{sqrtf, acosf, cosf, roundf};

use {defmt_rtt as _, panic_probe as _};


bind_interrupts!(struct Irqs {
    PIO0_IRQ_0 => PioInterruptHandler<PIO0>;
    USBCTRL_IRQ => UsbInterruptHandler<USB>;
});

#[embassy_executor::task]
async fn logger_task(driver: UsbDriver<'static, USB>) {
    embassy_usb_logger::run!(1024, log::LevelFilter::Info, driver);
}

const WIFI_NETWORK: &str = "J'ai la co et pas toi!!";
const WIFI_PASSWORD: &str = "internat123";

#[embassy_executor::task]
async fn wifi_task(
    runner: cyw43::Runner<'static, Output<'static, PIN_23>, PioSpi<'static, PIN_25, PIO0, 0, DMA_CH0>>,
) -> ! {
    runner.run().await
}

#[embassy_executor::task]
async fn net_task(stack: &'static Stack<cyw43::NetDriver<'static>>) -> ! {
    stack.run().await
}


fn calculate_angle(a: f32, b: f32, c: f32) -> f32 {
    let pre_angle = (a * a + b * b - c * c) / (2. * a * b);
    acosf(pre_angle)
}

fn calculate_length(a: f32, b: f32, angle: f32) -> f32 {
    sqrtf(a * a + b * b - 2. * a * b * cosf(angle))
}

#[embassy_executor::main]
async fn main(spawner: Spawner) {
    log::info!("Hello World!");

    let p = embassy_rp::init(Default::default());

    let driver = UsbDriver::new(p.USB, Irqs);
    unwrap!(spawner.spawn(logger_task(driver)));

    log::info!("Should be able to go farther");


    // turn on 5 leds
    let mut led1 = Output::new(p.PIN_2, Level::Low);
    let mut led2 = Output::new(p.PIN_3, Level::Low);
    let mut led3 = Output::new(p.PIN_4, Level::Low);
    let mut led4 = Output::new(p.PIN_5, Level::Low);
    let mut led5 = Output::new(p.PIN_6, Level::Low);

    let fw = include_bytes!("../../embassy_cyw/cyw43-firmware/43439A0.bin");
    let clm = include_bytes!("../../embassy_cyw/cyw43-firmware/43439A0_clm.bin");

    // To make flashing faster for development, you may want to flash the firmwares independently
    // at hardcoded addresses, instead of baking them into the program with `include_bytes!`:
    //     probe-rs download 43439A0.bin --format bin --chip RP2040 --base-address 0x10100000
    //     probe-rs download 43439A0_clm.bin --format bin --chip RP2040 --base-address 0x10140000
    //let fw = unsafe { core::slice::from_raw_parts(0x10100000 as *const u8, 230321) };
    //let clm = unsafe { core::slice::from_raw_parts(0x10140000 as *const u8, 4752) };

    let pwr = Output::new(p.PIN_23, Level::Low);
    let pwr_second = Output::new(p.PIN_26, Level::Low);

    let cs = Output::new(p.PIN_25, Level::High);

    let mut pio = Pio::new(p.PIO0, Irqs);
    let spi = PioSpi::new(&mut pio.common, pio.sm0, pio.irq0, cs, p.PIN_24, p.PIN_29, p.DMA_CH0);

    let state = make_static!(cyw43::State::new());
    let (net_device, mut control, runner) = cyw43::new(state, pwr, spi, fw).await;
    unwrap!(spawner.spawn(wifi_task(runner)));

    control.init(clm).await;
    control
        .set_power_management(cyw43::PowerManagementMode::None)
        .await;

    let config = Config::dhcpv4(Default::default());
    //let config = embassy_net::Config::ipv4_static(embassy_net::StaticConfigV4 {
    //    address: Ipv4Cidr::new(Ipv4Address::new(192, 168, 69, 2), 24),
    //    dns_servers: Vec::new(),
    //    gateway: Some(Ipv4Address::new(192, 168, 69, 1)),
    //});

    // Generate random seed
    let seed = 0x0123_4567_89ab_cdef; // chosen by fair dice roll. guarenteed to be random.

    // Init network stack
    let stack = &*make_static!(Stack::new(
        net_device,
        config,
        make_static!(StackResources::<2>::new()),
        seed
    ));

    unwrap!(spawner.spawn(net_task(stack)));

    let mut l_esp1_esp2: f32 = 40.;
    let mut count = 0;

    loop {
        //control.join_open(WIFI_NETWORK).await;
        match control.join_wpa2(WIFI_NETWORK, WIFI_PASSWORD).await {
            Ok(_) => break,
            Err(err) => {
                log::info!("join failed with status={}", err.status);
            }
        }
    }

    // Wait for DHCP, not necessary when using static IP
    log::info!("waiting for DHCP...");
    while !stack.is_config_up() {
        Timer::after_millis(100).await;
    }
    log::info!("DHCP is now up!");

    let a = stack.config_v4().unwrap().address;
    log::info!("My address : {:?}", a.address().0);

    // And now we can use it!

    let mut rx1 = [0; 16];
    let mut tx1 = [0; 16];

    let mut l_esp1_py: f32 = 0.;
    let mut l_esp2_py: f32 = 0.;

    let mut port: u16 = 1234;

    loop {
        Timer::after(Duration::from_millis(100)).await;

        control.gpio_set(0, false).await;

        let mut socket = TcpSocket::new(stack, &mut rx1, &mut tx1);
        socket.set_timeout(Some(Duration::from_secs(10)));
        log::info!("Listening on TCP:1234 for socket {}", port);
        if let Err(e) = socket.accept(port).await {
            log::warn!("accept error socket1: {:?}", e);
            continue;
        }
        log::info!("Received connection from socket1 {:?}", socket.remote_endpoint());

        control.gpio_set(0, true).await;

        log::info!("No Sleeping for a while");


        log::info!("Woke up !!!");

        loop
        {
            let mut send_ack: Result<usize, Error>;

            loop
            {
                log::info!("Ask Socket {}", port);
                send_ack = socket.write("send".as_bytes()).await;

                let Ok(()) = socket.flush().await else {
                    println!("Socket couldn't be flushed");
                    break;
                };

                log::info!("Wait for data Socket {}", port);

                let mut buf = [0; 16];
                match socket.read(&mut buf).await {
                    Ok(0) => {
                        log::info!("It read 0 byte, continuing");
                        continue;
                    }
                    Err(e) => {
                        log::info!("It failed to read from socket, continuing");
                        continue;
                    }
                    Ok(n) => {
                        /* Averages the value of the rssi received into l_espX_py */
                        log::info!("Here's the reception (sock1): {}, with n: {}", from_utf8(&buf[..n]).unwrap(), n);
                        let mut i = n;

                        if port == 1234 {
                            l_esp1_py = 0.;
                            while i > 0 {
                                l_esp1_py += buf[i] as f32;
                                i -= 1;
                            }

                            l_esp1_py = l_esp1_py / n as f32;
                        } else {
                            l_esp2_py = 0.;
                            while i > 0 {
                                l_esp2_py += buf[i] as f32;
                                i -= 1;
                            }

                            l_esp2_py = l_esp2_py / n as f32;
                        }

                        break;
                    }
                }
            }

            log::info!("Here is the average rssi of socket1: {}", l_esp1_py);
            log::info!("Here is the average rssi of socket2: {}", l_esp2_py);

            // Do the math here
            let l_Py_ESP1_ESP2_Radians: f32 = calculate_angle(l_esp1_esp2, l_esp1_py, l_esp2_py);
            log::info!("The angle (Py ESP1 ESP2) is: {}", roundf(l_Py_ESP1_ESP2_Radians * (180. / PI as f32)));
            let l_Rasp_Py: f32 = calculate_length(l_esp1_esp2 / 2., l_esp1_py, l_Py_ESP1_ESP2_Radians);
            log::info!("The distance between the Rasp and the Py is: {l_Rasp_Py}.");
            let l_ESP1_Rasp_Py_Radians: f32 = calculate_angle(l_esp1_esp2 / 2., l_Rasp_Py, l_esp1_py);
            log::info!("The angle (ESP1 Rasp Py) is: {}", roundf(l_ESP1_Rasp_Py_Radians * (180. / PI as f32)));
            let l_ESP1_Rasp_Py_Degree: f32 = l_ESP1_Rasp_Py_Radians * (180. / PI as f32);

            log::info!("The distance between the Rasp and the Py is: {l_Rasp_Py}.");
            log::info!("The angle (ESP1 Rasp Py) is: {}", roundf(l_ESP1_Rasp_Py_Degree));
            count += 1;

            if (count <= 5) {
                l_esp1_esp2 = sqrtf(l_esp1_py * l_esp1_py + l_esp2_py * l_esp2_py);
                log::info!("The distance between ESP1 and ESP2 is supposed to be : {}", l_esp1_esp2);
                l_esp1_esp2 *= 0.4;
                log::info!("The distance between ESP1 and ESP2 is calibrated to: {}", l_esp1_esp2);
            }



            /* Handle leds (from 0 to 180, there is 5 leds, a single led on at a time to point the direction) */
            // if (l_ESP1_Rasp_Py_Degree < 50.) {
            //     led1.set_high();
            //     led2.set_low();
            //     led3.set_low();
            //     led4.set_low();
            //     led5.set_low();
            // } else if (l_ESP1_Rasp_Py_Degree < 72.) {
            //     led1.set_low();
            //     led2.set_high();
            //     led3.set_low();
            //     led4.set_low();
            //     led5.set_low();
            // } else if (l_ESP1_Rasp_Py_Degree < 108.) {
            //     led1.set_low();
            //     led2.set_low();
            //     led3.set_high();
            //     led4.set_low();
            //     led5.set_low();
            // } else if (l_ESP1_Rasp_Py_Degree < 130.) {
            //     led1.set_low();
            //     led2.set_low();
            //     led3.set_low();
            //     led4.set_high();
            //     led5.set_low();
            // } else {
            //     led1.set_low();
            //     led2.set_low();
            //     led3.set_low();
            //     led4.set_low();
            //     led5.set_high();
            // }


            if (l_ESP1_Rasp_Py_Degree < 40.0) {
                led1.set_high();
                led2.set_low();
                led3.set_low();
                led4.set_low();
                led5.set_low();
            } else if (l_ESP1_Rasp_Py_Degree < 60.0) {
                led1.set_high();
                led2.set_high();
                led3.set_low();
                led4.set_low();
                led5.set_low();
            } else if (l_ESP1_Rasp_Py_Degree < 75.0) {
                led1.set_low();
                led2.set_high();
                led3.set_low();
                led4.set_low();
                led5.set_low();
            } else if (l_ESP1_Rasp_Py_Degree < 87.5) {
                led1.set_low();
                led2.set_high();
                led3.set_high();
                led4.set_low();
                led5.set_low();
            } else if (l_ESP1_Rasp_Py_Degree < 92.5) {
                led1.set_low();
                led2.set_low();
                led3.set_high();
                led4.set_low();
                led5.set_low();
            } else if (l_ESP1_Rasp_Py_Degree < 105.0) {
                led1.set_low();
                led2.set_low();
                led3.set_high();
                led4.set_high();
                led5.set_low();
            } else if (l_ESP1_Rasp_Py_Degree < 120.0) {
                led1.set_low();
                led2.set_low();
                led3.set_low();
                led4.set_high();
                led5.set_low();
            } else if (l_ESP1_Rasp_Py_Degree < 140.0) {
                led1.set_low();
                led2.set_low();
                led3.set_low();
                led4.set_high();
                led5.set_high();
            } else {
                led1.set_low();
                led2.set_low();
                led3.set_low();
                led4.set_low();
                led5.set_high();
            }


            socket.close();
            if port == 1234 {
                port = 1248;
            } else {
                port = 1234;
            }

            break;
        }
    }
}
