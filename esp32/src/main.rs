#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use core::str::{from_utf8, Utf8Error};
use embassy_executor::Spawner;
use embassy_net::tcp::TcpSocket;
use embassy_net::{Config, Ipv4Address, Stack, StackResources};
use esp32c3_hal as hal;

use embassy_time::{Duration, Timer};
use embedded_io_async::Write;
use embedded_svc::wifi::{AccessPointInfo, ClientConfiguration, Configuration, Wifi};
use esp_backtrace as _;
use esp_println::println;
use esp_wifi::wifi::{WifiController, WifiDevice, WifiStaDevice, WifiState};
use esp_wifi::{initialize, EspWifiInitFor};
use esp_wifi::wifi::utils::create_network_interface;
use hal::clock::ClockControl;
use hal::Rng;
use hal::{embassy, peripherals::Peripherals, prelude::*, timer::TimerGroup};
use static_cell::make_static;
use esp_wifi::wifi::WifiError;
use heapless::mpmc::{Q16, Q2, Q4};
use heapless::Vec;
use smoltcp::iface::SocketStorage;


const SSID: &str = "J'ai la co et pas toi!!";
const PASSWORD: &str = "internat123";

const WAIT_ENQUEUE: u64 = 15;

static SHARED_RSSI: Q4<i8> = Q4::new();


#[main(entry = "esp_riscv_rt::entry")]
async fn main(spawner: Spawner) -> ! {
    #[cfg(feature = "log")]
    esp_println::logger::init_logger(log::LevelFilter::Info);

    let peripherals = Peripherals::take();

    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::max(system.clock_control).freeze();

    #[cfg(target_arch = "riscv32")]
        let timer = hal::systimer::SystemTimer::new(peripherals.SYSTIMER).alarm0;
    let init = initialize(
        EspWifiInitFor::Wifi,
        timer,
        Rng::new(peripherals.RNG),
        system.radio_clock_control,
        &clocks,
    )
        .unwrap();

    let wifi = peripherals.WIFI;
    let mut socket_set_entries: [SocketStorage; 3] = Default::default();

    let (_wifi_interface, device, controller, _) =
        create_network_interface(&init, wifi, WifiStaDevice, &mut socket_set_entries).unwrap();

    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    embassy::init(&clocks, timer_group0.timer0);

    let config = Config::dhcpv4(Default::default());

    let seed = 1234; // very random, very secure seed

    // Init network stack
    let stack = &*make_static!(Stack::new(
        device,
        config,
        make_static!(StackResources::<3>::new()),
        seed
    ));

    spawner.spawn(connection(controller)).ok();
    spawner.spawn(net_task(&stack)).ok();

    let mut rx_buffer = [0; 4096];
    let mut tx_buffer = [0; 4096];

    loop {
        if stack.is_link_up() {
            break;
        }
        Timer::after(Duration::from_millis(500)).await;
    }

    println!("Waiting to get IP address...");
    loop {
        if let Some(config) = stack.config_v4() {
            println!("Got IP: {}", config.address);
            break;
        }
        Timer::after(Duration::from_millis(500)).await;
    }

    loop {
        Timer::after(Duration::from_millis(1_000)).await;

        let mut socket = TcpSocket::new(&stack, &mut rx_buffer, &mut tx_buffer);

        socket.set_timeout(Some(embassy_time::Duration::from_secs(8)));

        let remote_endpoint = (Ipv4Address::new(192, 168, 1, 10), 1248);
        println!("connecting...");
        let r = socket.connect(remote_endpoint).await;
        if let Err(e) = r {
            println!("connect error: {:?}", e);
            continue;
        }
        println!("connected!");
        let mut rx_buf = [0; 16];
        let mut tx_buf = [0; 16];

        loop
        {
            // use embedded_io_async::Write;
            println!("Waiting to get something to do...");
            let Ok(msg_size) = socket.read(&mut rx_buf).await else {
                /* Connection broke? (break -> remake the socket) */
                println!("socket.read() error");
                break;
            };

            println!("Asked me to do smth");
            let mut vec: Vec<u8, 4> = Vec::new();

            while !vec.is_full() {
                let mut is_full: bool = false;
                while let Some(item) = SHARED_RSSI.dequeue() {
                    let a = if item.is_positive() { item } else { -item } as u8;
                    vec.push(a);

                    // stop if the vector will be full
                    if vec.is_full() {
                        break;
                    }
                }

                // Wait 10 ms to hopefully refill the queue if it wasn't full
                if !vec.is_full() {
                    Timer::after(Duration::from_millis(40)).await;
                }
            }

            let slice: &[u8] = vec.as_slice();
            println!("La slice : {}", from_utf8(slice).unwrap());

            let r = socket.write(slice).await;
            if let Err(e) = r {
                println!("Socket write error");
                /* Leave to remake the socket (shouldn't write error because our buffer is good, but happens if the socket is dead) */
                break;
            } else {
                println!("Data sent without any writing error");
                /*let Ok(()) = socket.flush().await else {
                    println!("Socket couldn't be flushed");
                    break;
                };*/
                //println!("Post flush");
            }
        };
    }
}


#[embassy_executor::task]
async fn connection(mut controller: WifiController<'static>) {
    println!("start connection task");
    println!("Device capabilities: {:?}", controller.get_capabilities());
    loop {
        match esp_wifi::wifi::get_wifi_state() {
            WifiState::StaConnected => {


                // controller.start().await.unwrap();
                // println!("Wifi started!");

                //println!("Start Wifi Scan");
                let res: Result<(heapless::Vec<AccessPointInfo, 10>, usize), WifiError> = controller.scan_n().await;
                if let Ok((res, _count)) = res {
                    for ap in res {
                        if ap.ssid == "trian_target" {
                            //println!("Signal Strength {:?}", ap.signal_strength);

                            let enqueued_value = SHARED_RSSI.enqueue(ap.signal_strength);
                            match enqueued_value {
                                Ok(()) => {}
                                Err(item) => {
                                    //println!("Had to dequeue;");
                                    let _ = SHARED_RSSI.dequeue();
                                    let _ = SHARED_RSSI.enqueue(ap.signal_strength);
                                }
                            }
                        }
                    }
                }
                // controller.wait_for_event(WifiEvent::StaDisconnected).await;
                Timer::after(Duration::from_millis(WAIT_ENQUEUE)).await;
                continue;
            }
            _ => {}
        }
        if !matches!(controller.is_started(), Ok(true)) {
            let client_config = Configuration::Client(ClientConfiguration {
                ssid: SSID.try_into().unwrap(),
                password: PASSWORD.try_into().unwrap(),
                ..Default::default()
            });

            controller.set_configuration(&client_config).unwrap();


            println!("Starting wifi");
            controller.start().await.unwrap();
            println!("Wifi started!");
        }
        println!("About to connect...");

        match controller.connect().await {
            Ok(_) => println!("Wifi connected!"),
            Err(e) => {
                println!("Failed to connect to wifi: {e:?}");
                Timer::after(Duration::from_millis(5000)).await
            }
        }
    }
}

#[embassy_executor::task]
async fn net_task(stack: &'static Stack<WifiDevice<'static, WifiStaDevice>>) {
    stack.run().await
}
